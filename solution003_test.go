package go_euler

import "testing"
import "fmt"

func TestSolution003(t *testing.T) {
	expected := 6857
	actual := Solution003()
	if actual != expected {
		t.Errorf("Solution003 == %d, expected %d", actual, expected)
	}
  fmt.Printf("Solution003 %d\n", actual)
}
