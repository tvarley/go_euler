package go_euler

import "testing"
import "fmt"

func TestSolution004(t *testing.T) {
	expected := 906609
	actual := Solution004()
	if actual != expected {
		t.Errorf("Solution004 == %d, expected %d", actual, expected)
	}
  fmt.Printf("Solution004 %d\n", actual)
}
